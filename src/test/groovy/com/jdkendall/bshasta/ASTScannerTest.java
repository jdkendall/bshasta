package com.jdkendall.bshasta;

import bsh.EvalError;
import bsh.ParseException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ASTScannerTest {
    @Test
    void inner_scope_variables_caught_out_of_scope() throws ParseException {
        String script = """
                name = "foo";
                ban = null;
                if(name == null) {
                    foo = true;
                    int jar = 10;
                }

                for(int x = 0;
                    x < 10;
                    x++) {
                  bar = x;
                }

                while(name != null) {
                  baz = name;
                  name = null;
                }

                do {
                  bag = name;
                  name = null;
                } while(name == null);

                print(x); // Should be out of scope after for loop ends
                print(foo);
                print(bar);
                print(baz);
                print(bag);
                print(ban);
                print(jar);
                """;

        ASTScanner testee = new ASTScanner();
        List<ErrorCase> result = testee.scanForErrors(script);
        List<ErrorCase> expectedErrors = List.of(
                new ErrorCase("x", 24),
                new ErrorCase("foo", 25),
                new ErrorCase("bar", 26),
                new ErrorCase("baz", 27),
                new ErrorCase("bag", 28),
                new ErrorCase("jar", 30));
        assertEquals(expectedErrors, result);
    }

    @Test
    void imports_flagged_correctly() throws ParseException {
        String script = """
                import com.github.javaparser.utils.Pair;

                abc = new Pair<String, String>("a", "b");
                def = new Foobar(abc, ghi);

                print(abc);
                print(def);
                print(ghi);
                """;

        ASTScanner testee = new ASTScanner();
        List<ErrorCase> result = testee.scanForErrors(script);
        List<ErrorCase> expectedErrors = List.of(
                new ErrorCase("Foobar", 4),
                new ErrorCase("ghi", 4),
                new ErrorCase("ghi", 8));
        assertEquals(expectedErrors, result);
    }

    @Test
    void method_accesses_identify_vars() throws ParseException {
        String script = """
                abc = "foo";
                def = abc.getLength();
                ghi.getData().bar = def;
                ghi.getData().getOtherData().bar = def;

                print(abc);
                print(def);
                print(ghi);
                print(ghi.getData().bar);
                """;

        ASTScanner testee = new ASTScanner();
        List<ErrorCase> result = testee.scanForErrors(script);
        List<ErrorCase> expectedErrors = List.of(
                new ErrorCase("ghi", 3),
                new ErrorCase("ghi", 4),
                new ErrorCase("ghi", 8),
                new ErrorCase("ghi", 9));
        assertEquals(expectedErrors, result);
    }

    @Test
    void verify_weird_places_for_assignment() throws EvalError {
        String script = """
                abc = def = "foo";
                if((ghi = true) == true) {
                    jkl = new ArrayList<Integer>();
                    jkl.add(lmn = 5);
                    print(jkl);
                    print(lmn);
                }

                print(abc);
                print(def);
                print(ghi);
                print(jkl);
                print(lmn);
                """;

        ASTScanner testee = new ASTScanner();
        testee.addToGlobalScope("ArrayList");
        List<ErrorCase> result = testee.scanForErrors(script);
        List<ErrorCase> expectedErrors = List.of(
                new ErrorCase("ghi", 11),
                new ErrorCase("jkl", 12),
                new ErrorCase("lmn", 13));
        assertEquals(expectedErrors, result);
    }
}
