package com.jdkendall.bshasta


import com.jdkendall.bshasta.db.ScriptRepository

static void main(String[] args) {
    ScriptRepository.wipeScripts()
    ScriptRepository.addScript("Foo",
            """
                 import com.github.javaparser.utils.Pair;
                 
                 name = "foo";
                 if(name != null) {
                   bar = new Pair("a", "b");
                   print("hello!");
                 } 
                 
                 print(bar == null);
                 print(bar != null);
                 
                 if(name != null && bar != null && "b".equals(bar.toString())) {
                   print("bar!");
                 }
                 
                 print(baz);
                 print("goodbye!");
                 """)
    ASTScanner astScanner = new ASTScanner();
    for (script in ScriptRepository.loadScripts()) {
        astScanner.scanForErrors(script.data())
    }
}

