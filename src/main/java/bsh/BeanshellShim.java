package bsh;

public class BeanshellShim {
    public static String getNameFromVariableDeclarator(Node node) {
        BSHVariableDeclarator declarator = (BSHVariableDeclarator) node;
        return declarator.name;
    }

    public static String getPrimaryExpressionVarName(Node node) {
        BSHPrimaryExpression expr = (BSHPrimaryExpression) node;
        return expr.firstToken.image;
    }

    public static String getMethodInvocationName(Node node) {
        BSHMethodInvocation expr = (BSHMethodInvocation) node;
        return expr.getNameNode().firstToken.image;
    }

    public static boolean isMethodCall(Node node) {
        BSHMethodInvocation expr = (BSHMethodInvocation) node;
        BSHAmbiguousName nameNode = expr.getNameNode();
        return nameNode.firstToken != nameNode.lastToken;
    }
}
