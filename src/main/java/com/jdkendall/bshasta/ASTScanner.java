package com.jdkendall.bshasta;

import bsh.*;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.*;

public class ASTScanner {
    private final Set<String> globalScope = new HashSet<>();

    public List<ErrorCase> scanForErrors(String script) throws ParseException {
        var cs = Charset.defaultCharset();
        Parser parser = new Parser(new ByteArrayInputStream(script.getBytes(cs)), cs);
        Stack<Set<String>> scopeStack = new Stack<>();
        List<ErrorCase> errorCases = new ArrayList<>();
        scopeStack.push(new HashSet<>(globalScope));
        while (!parser.Line()) {
            var node = parser.popNode();
            parseNode(node, scopeStack, errorCases);
        }

        errorCases.forEach(_case -> System.out.println(_case.toString()));
        return errorCases;
    }

    private void parseNode(Node node, Stack<Set<String>> scopeStack, List<ErrorCase> errorCases) {
        switch (node.getId()) {
            case ParserTreeConstants.JJTIMPORTDECLARATION:
                for (Node child : node.jjtGetChildren()) {
                    if (child.getId() == ParserTreeConstants.JJTAMBIGUOUSNAME) {
                        String[] importSplit = child.getText().split("\\.");
                        String importName = importSplit[importSplit.length - 1].strip();
                        System.out.printf("Found import for %s%n", importName);
                        scopeStack.peek().add(importName);
                    }
                }
                break;

            case ParserTreeConstants.JJTMETHODINVOCATION:
                // Not checking method names at this time, so skip to analyzing args
                if (node.jjtGetNumChildren() > 1) {
                    parseNode(node.jjtGetChild(1), scopeStack, errorCases);
                }
                break;

            case ParserTreeConstants.JJTENHANCEDFORSTATEMENT:
            case ParserTreeConstants.JJTWHILESTATEMENT:
            case ParserTreeConstants.JJTFORSTATEMENT:
            case ParserTreeConstants.JJTIFSTATEMENT:
            case ParserTreeConstants.JJTTRYWITHRESOURCES:
            case ParserTreeConstants.JJTMULTICATCH:
            case ParserTreeConstants.JJTBLOCK:
                System.out.println("New scope");
                scopeStack.push(new HashSet<>());
                for (Node child : node.jjtGetChildren()) {
                    parseNode(child, scopeStack, errorCases);
                }
                scopeStack.pop();
                break;


            case ParserTreeConstants.JJTVARIABLEDECLARATOR: {
                String varName = BeanshellShim.getNameFromVariableDeclarator(node).strip();
                System.out.printf("Found variable declaration for %s%n", varName);
                scopeStack.peek().add(varName);

                for (Node child : node.jjtGetChildren()) {
                    parseNode(child, scopeStack, errorCases);
                }
            }
            break;

            case ParserTreeConstants.JJTASSIGNMENT:
                var firstChild = node.jjtGetChild(0);
                if (firstChild.getId() == ParserTreeConstants.JJTPRIMARYEXPRESSION) {
                    var child = firstChild.jjtGetChild(0);
                    if (child.getId() == ParserTreeConstants.JJTAMBIGUOUSNAME) {
                        String varName = child.getText().strip();
                        if (node.jjtGetNumChildren() > 1) {
                            // Extract variable name and store it in scope
                            System.out.printf("Found assignment to %s%n", varName);
                            scopeStack.peek().add(varName);
                        } else {
                            if (notInScope(scopeStack, varName)) {
                                errorCases.add(new ErrorCase(varName, node.getLineNumber()));
                            }
                        }
                    } else if (child.getId() == ParserTreeConstants.JJTMETHODINVOCATION) {
                        if(BeanshellShim.isMethodCall(child)) {
                            String varName = BeanshellShim.getPrimaryExpressionVarName(firstChild);
                            if (notInScope(scopeStack, varName)) {
                                errorCases.add(new ErrorCase(varName, firstChild.getLineNumber()));
                            }
                        }
                        parseNode(firstChild, scopeStack, errorCases);
                    } else {
                        parseNode(child, scopeStack, errorCases);
                    }

                    for (Node c : node.jjtGetChildren()) {
                        if (c != firstChild) {
                            parseNode(c, scopeStack, errorCases);
                        }
                    }
                } else if (firstChild.getId() == ParserTreeConstants.JJTBINARYEXPRESSION) {
                    for (Node child : firstChild.jjtGetChildren()) {
                        parseNode(child, scopeStack, errorCases);
                    }
                }
                break;

            case ParserTreeConstants.JJTAMBIGUOUSNAME:
                String varName = node.getText().strip();
                if (notInScope(scopeStack, varName)) {
                    errorCases.add(new ErrorCase(varName, node.getLineNumber()));
                }
                break;
            default:
                for (Node child : node.jjtGetChildren()) {
                    parseNode(child, scopeStack, errorCases);
                }
                break;
        }
    }

    private boolean notInScope(Stack<Set<String>> scopeStack, String identifier) {
        return scopeStack.stream().noneMatch(scope -> scope.contains(identifier));
    }

    public void addToGlobalScope(String name) {
        globalScope.add(name);
    }
}
