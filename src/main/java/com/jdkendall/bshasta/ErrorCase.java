package com.jdkendall.bshasta;

public record ErrorCase(String variableName, int lineNumber) {
    @Override
    public String toString() {
        return "%s:L%d".formatted(variableName, lineNumber);
    }
}
