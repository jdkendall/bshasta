package com.jdkendall.bshasta;

public record BshScript(int id, String name, String data) {
}
