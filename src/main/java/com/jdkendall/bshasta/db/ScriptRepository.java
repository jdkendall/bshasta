package com.jdkendall.bshasta.db;

import com.github.javaparser.utils.Pair;
import com.jdkendall.bshasta.BshScript;
import org.postgresql.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ScriptRepository {
    record Credentials(String url, String user, String password) {}
    static Credentials CREDS = new Credentials("jdbc:postgresql://localhost:5432/postgres","postgres","pass");

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    // Loads an array of Beanshell scripts from Postgres.
    // The scripts are returned as a list.
    public static List<BshScript> loadScripts() {
        // Connect to Postgres DB
        try(Connection dbConn = DriverManager.getConnection(CREDS.url(), CREDS.user(), CREDS.password());
            PreparedStatement pStmt = dbConn.prepareStatement("SELECT id, name, script_data FROM scripts");
            ResultSet result = pStmt.executeQuery()) {

            List<BshScript> scripts = new ArrayList<>();
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String data = result.getString("script_data");
                scripts.add(new BshScript(id, name, data));
            }

            return scripts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void wipeScripts() {
        // Connect to Postgres DB
        try(Connection dbConn = DriverManager.getConnection(CREDS.url(), CREDS.user(), CREDS.password());
            PreparedStatement pStmt = dbConn.prepareStatement("DELETE FROM scripts")) {
            pStmt.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void addScript(String name, String data) {
        // Connect to Postgres DB
        try(Connection dbConn = DriverManager.getConnection(CREDS.url(), CREDS.user(), CREDS.password());
            PreparedStatement pStmt = dbConn.prepareStatement("INSERT INTO scripts (name, script_data) VALUES (?, ?)")) {
            pStmt.setString(1, name);
            pStmt.setString(2, data);
            pStmt.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
